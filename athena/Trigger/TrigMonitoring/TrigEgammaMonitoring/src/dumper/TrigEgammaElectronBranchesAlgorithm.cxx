

#include "TrigEgammaElectronBranchesAlgorithm.h"


TrigEgammaElectronBranchesAlgorithm::TrigEgammaElectronBranchesAlgorithm( const std::string& name, ISvcLocator* pSvcLocator ):
  TrigEgammaMonitorBaseAlgorithm( name, pSvcLocator )

{}

TrigEgammaElectronBranchesAlgorithm::~TrigEgammaElectronBranchesAlgorithm()
{}


StatusCode TrigEgammaElectronBranchesAlgorithm::initialize() 
{
  
  ATH_CHECK(TrigEgammaMonitorBaseAlgorithm::initialize());
 
  return StatusCode::SUCCESS;
}


// *********************************************************************************


void TrigEgammaElectronBranchesAlgorithm::fillLabel( const ToolHandle<GenericMonitoringTool>& groupHandle, 
                                                    const std::string &histname, 
                                                    const std::string &label ) const
{
  auto mon = Monitored::Scalar<std::string>( histname, label );
  fill( groupHandle, mon );
}



// *********************************************************************************


void TrigEgammaElectronBranchesAlgorithm::fillFastCalo(  const ToolHandle<GenericMonitoringTool>& groupHandle, const xAOD::TrigEMCluster *cl ) const

{
  auto mon_eta = Monitored::Scalar<float>("trig_L2_cl_eta" , cl->eta()     );
}


void TrigEgammaElectronBranchesAlgorithm::fillOffline( const ToolHandle<GenericMonitoringTool>& groupHandle, const xAOD::Electron *el ) const
{
  auto mon_et  = Monitored::Scalar<float>("el_et"  , el->pt()      );
  auto mon_eta = Monitored::Scalar<float>("el_eta" , el->eta()     );
  auto mon_phi = Monitored::Scalar<float>("el_phi" , el->phi()     );

  fill(groupHandle, mon_et, mon_eta, mon_phi);
}