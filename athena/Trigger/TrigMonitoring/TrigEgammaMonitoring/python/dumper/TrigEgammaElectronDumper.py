# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration

'''@file TrigEgammaMonitoringConfigRun3.py
@author D. Maximov (histograms), Joao victor Pinto (core)
@date 2019-07-08
@brief Run 3 configuration builder. Histograms definitions taken from TrigEgammaPlotTool
'''

from ElectronPhotonSelectorTools.TrigEGammaPIDdefs import SelectionDefPhoton
import cppyy
import functools
 
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentFactory import CompFactory as CfgMgr



def defineBranch( branch, typename ):
    return branch+'/'+typename

def generateTreeBranches( branches, treename ):
    return ','.join( [ branch.split('/')[0] for branch in branches] ) + ';' + treename

def generateTreeDefs( branches ):
    return ':'.join(branches)




class TrigEgammaElectronDumperBuilder:



  isemnames = ["tight", "medium", "loose"]
  lhnames   = ["lhtight", "lhmedium", "lhloose","lhvloose"]
  dnnnames  = ["dnntight", "dnnmedium", "dnnloose"]


  def __init__(self, helper, runflag, basePath = 'HLT/EgammaMon'):
 
    from AthenaCommon.Logging import logging
    self.__logger = logging.getLogger( 'TrigEgammaMonAlgBuilder' )
    self.runFlag = runflag
    self.helper = helper
    self.basePath = basePath
    self.configure()
    


  #
  # Create all minitor algorithms
  #
  def configure( self ):
   
    acc = self.helper.resobj
    EgammaMatchTool = CompFactory.TrigEgammaMatchingToolMT()
    EgammaMatchTool.DeltaR=0.4
    acc.addPublicTool(EgammaMatchTool)
    cppyy.load_library('libElectronPhotonSelectorToolsDict')
    # Following loads the online selectors
  
    # Offline selectors -- taken from latest conf
    LooseElectronSelector             = CfgMgr.AsgElectronIsEMSelector("T0HLTLooseElectronSelector")
    MediumElectronSelector            = CfgMgr.AsgElectronIsEMSelector("T0HLTMediumElectronSelector")
    TightElectronSelector             = CfgMgr.AsgElectronIsEMSelector("T0HLTTightElectronSelector")
    LooseLHSelector                   = CfgMgr.AsgElectronLikelihoodTool("T0HLTLooseLHSelector")
    MediumLHSelector                  = CfgMgr.AsgElectronLikelihoodTool("T0HLTMediumLHSelector")
    TightLHSelector                   = CfgMgr.AsgElectronLikelihoodTool("T0HLTTightLHSelector")
    VeryLooseLHSelector               = CfgMgr.AsgElectronLikelihoodTool("T0HLTVeryLooseLHSelector")
 
    # DNN selectors 
    LooseDNNElectronSelector          = CfgMgr.AsgElectronSelectorTool("T0HLTLooseElectronDNNSelector")
    MediumDNNElectronSelector         = CfgMgr.AsgElectronSelectorTool("T0HLTMediumElectronDNNSelector")
    TightDNNElectronSelector          = CfgMgr.AsgElectronSelectorTool("T0HLTTightElectronDNNSelector")

    LoosePhotonSelector               = CfgMgr.AsgPhotonIsEMSelector( "T0HLTLoosePhotonSelector" )
    MediumPhotonSelector              = CfgMgr.AsgPhotonIsEMSelector( "T0HLTMediumPhotonSelector" )
    TightPhotonSelector               = CfgMgr.AsgPhotonIsEMSelector( "T0HLTTightPhotonSelector" )

    LoosePhotonSelector.ForceConvertedPhotonPID = True
    LoosePhotonSelector.isEMMask = SelectionDefPhoton.PhotonLoose
    MediumPhotonSelector.ForceConvertedPhotonPID = True
    MediumPhotonSelector.isEMMask = SelectionDefPhoton.PhotonMedium
    TightPhotonSelector.ForceConvertedPhotonPID = True
    TightPhotonSelector.isEMMask = SelectionDefPhoton.PhotonTight


    acc.addPublicTool(LooseElectronSelector)
    acc.addPublicTool(MediumElectronSelector)
    acc.addPublicTool(TightElectronSelector)
    acc.addPublicTool(LooseLHSelector)
    acc.addPublicTool(MediumLHSelector)
    acc.addPublicTool(TightLHSelector)
    acc.addPublicTool(VeryLooseLHSelector)
    acc.addPublicTool(LooseDNNElectronSelector)
    acc.addPublicTool(MediumDNNElectronSelector)
    acc.addPublicTool(TightDNNElectronSelector)

    if self.runFlag == '2022':
      raise RuntimeError( '2022 (Run 3) configuration not available yet' )

  
    elif self.runFlag == '2018':
      # cut based
      LooseElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronIsEMLooseSelectorCutDefs.conf"
      MediumElectronSelector.ConfigFile = "ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronIsEMMediumSelectorCutDefs.conf"
      TightElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronIsEMTightSelectorCutDefs.conf"
      # 2018 (vtest)
      LooseLHSelector.ConfigFile        = "ElectronPhotonSelectorTools/offline/mc20_20210514/ElectronLikelihoodLooseOfflineConfig2017_CutBL_Smooth.conf"
      MediumLHSelector.ConfigFile       = "ElectronPhotonSelectorTools/offline/mc20_20210514/ElectronLikelihoodMediumOfflineConfig2017_Smooth.conf"
      TightLHSelector.ConfigFile        = "ElectronPhotonSelectorTools/offline/mc20_20210514/ElectronLikelihoodTightOfflineConfig2017_Smooth.conf"
      VeryLooseLHSelector.ConfigFile    = "ElectronPhotonSelectorTools/offline/mc20_20210514/ElectronLikelihoodVeryLooseOfflineConfig2017_Smooth.conf"
      # DNN
      LooseDNNElectronSelector.ConfigFile   = "ElectronPhotonSelectorTools/offline/mc16_20210430/ElectronDNNMulticlassLoose.conf"
      MediumDNNElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc16_20210430/ElectronDNNMulticlassMedium.conf"
      TightDNNElectronSelector.ConfigFile   = "ElectronPhotonSelectorTools/offline/mc16_20210430/ElectronDNNMulticlassTight.conf"
      # cutbased for photons
      TightPhotonSelector.ConfigFile    = "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf"
      MediumPhotonSelector.ConfigFile   = "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMMediumSelectorCutDefs.conf"
      LoosePhotonSelector.ConfigFile    = "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf"
      
    elif self.runFlag == '2017':
      # cut based
      LooseElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronIsEMLooseSelectorCutDefs.conf"
      MediumElectronSelector.ConfigFile = "ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronIsEMMediumSelectorCutDefs.conf"
      TightElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronIsEMTightSelectorCutDefs.conf"
      # 2017 (v11)
      LooseLHSelector.ConfigFile        = "ElectronPhotonSelectorTools/offline/mc15_20160512/ElectronLikelihoodLooseOfflineConfig2016_CutBL_Smooth.conf"
      MediumLHSelector.ConfigFile       = "ElectronPhotonSelectorTools/offline/mc15_20160512/ElectronLikelihoodMediumOfflineConfig2016_Smooth.conf"
      TightLHSelector.ConfigFile        = "ElectronPhotonSelectorTools/offline/mc15_20160512/ElectronLikelihoodTightOfflineConfig2016_Smooth.conf"
      VeryLooseLHSelector.ConfigFile    = "ElectronPhotonSelectorTools/offline/mc15_20160512/ElectronLikelihoodVeryLooseOfflineConfig2016_Smooth.conf"
      # DNN
      LooseDNNElectronSelector.ConfigFile   = "ElectronPhotonSelectorTools/offline/mc16_20210430/ElectronDNNMulticlassLoose.conf"
      MediumDNNElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc16_20210430/ElectronDNNMulticlassMedium.conf"
      TightDNNElectronSelector.ConfigFile   = "ElectronPhotonSelectorTools/offline/mc16_20210430/ElectronDNNMulticlassTight.conf"
      # cut based for photons 
      TightPhotonSelector.ConfigFile    = "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf"
      MediumPhotonSelector.ConfigFile   = "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMMediumSelectorCutDefs.conf"
      LoosePhotonSelector.ConfigFile    = "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf"
    
    else:
      # raise since the configuration its not defined
      raise RuntimeError( 'Wrong run flag configuration' )
    


    self.core = self.helper.addAlgorithm( CompFactory.TrigEgammaElectronSelectionAlgorithm, "TrigEgammaElectronSelectionAlgorithm" )

    self.core.Analysis='Zee_ntuple'
    self.core.MatchTool = EgammaMatchTool
    self.core.TPTrigger=False
    self.core.ElectronKey = 'Electrons'
    self.core.isEMResultNames=self.isemnames
    self.core.LHResultNames=self.lhnames
    self.core.DNNResultNames=self.dnnnames
    self.core.ElectronIsEMSelector =[TightElectronSelector,MediumElectronSelector,LooseElectronSelector]
    self.core.ElectronLikelihoodTool =[TightLHSelector,MediumLHSelector,LooseLHSelector,VeryLooseLHSelector]
    self.core.ElectronDNNSelectorTool =[TightDNNElectronSelector,MediumDNNElectronSelector,LooseDNNElectronSelector]
    self.core.ZeeLowerMass=80
    self.core.ZeeUpperMass=100
    self.core.OfflineTagMinEt=25
    self.core.OfflineTagSelector='lhtight'
    self.core.OfflineProbeSelector='lhloose'
    self.core.OppositeCharge=True
    self.core.RemoveCrack=False
    self.core.TagTriggerList=[]
    self.core.TriggerList=[]
    self.core.DoEmulation = False
    self.core.ApplyJetNearProbeSelection = False


    self.bookTree('events')


  # If we've already defined the group, return the object already defined
  @functools.lru_cache(None)
  def addGroup( self, monAlg, name, path ):
    return self.helper.addGroup( monAlg, name, path )

  def addHistogram(self, monGroup, hist):
      monGroup.defineHistogram(hist.name, **hist.kwargs)


 
  #
  # Monitoring histograms
  #


  def bookEvent(self, monAlg, analysis, tap=False):

    # Create mon group.  The group name should be the path name for map
    monGroup = self.addGroup( monAlg, analysis, self.basePath+'/Expert/Event/'+analysis )

    if tap:
      cutLabels = ["Events","LAr","RetrieveElectrons","TwoElectrons","PassTrigger","EventWise","Success"]
      probeLabels=["Electrons","NotTag","OS","SS","ZMass","HasTrack","HasCluster","Eta","Et","IsGoodOQ","GoodPid","NearbyJet","Isolated","GoodProbe"]
      tagLabels=["Electrons","HasTrack","HasCluster","GoodPid","Et","Eta","IsGoodOQ","PassTrigger","MatchTrigger"]

      monGroup.defineHistogram("CutCounter", type='TH1I', path='', title="Event Selection; Cut ; Count",
          xbins=len(cutLabels), xmin=0, xmax=len(cutLabels), xlabels=cutLabels)
      monGroup.defineHistogram("TagCutCounter", type='TH1F', path='', title="Number of Probes; Cut ; Count",
          xbins=len(tagLabels), xmin=0, xmax=len(tagLabels), xlabels=tagLabels)
      monGroup.defineHistogram("ProbeCutCounter", type='TH1F', path='', title="Number of Probes; Cut ; Count",
          xbins=len(probeLabels), xmin=0, xmax=len(probeLabels), xlabels=probeLabels)
      monGroup.defineHistogram("Mee", type='TH1F', path='', title="Offline M(ee); m_ee [GeV] ; Count",xbins=50, 
          xmin=monAlg.ZeeLowerMass, xmax=monAlg.ZeeUpperMass)






  #
  # book ntuple
  #
  def bookTree(self, treename):

    # Create mon group.  The group name should be the path name for map
    monGroup = self.addGroup( self.core, "ntuple", self.basePath )
    branches = self.bookOffline()
    branches+= self.bookFastCalo()
    
    print('AKI JOAO')
    print('monGroup.defineTree(%s, treedef=%s)'%(generateTreeBranches( branches, treename ),generateTreeDefs(branches)))

    monGroup.defineTree( generateTreeBranches( branches, treename ),
                         treedef=generateTreeDefs(branches) , path='summary',
                         )

  #
  # Branches
  #


  def bookOffline( self ):

    branches = [
              defineBranch( 'el_et' , 'F'),
              defineBranch( 'el_eta', 'F'),
              defineBranch( 'el_phi', 'F'),
    ]

    return branches

  #
  # generate branches
  #
  def bookFastCalo( self ):

    branches = [
            # TrigEMCluster
            #defineBranch( 'trig_L2_calo_et'            , 'F'), 
            defineBranch( 'trig_L2_calo_eta'           , 'F'), 
            #defineBranch( 'trig_L2_calo_phi'           , 'F'), 
            #defineBranch( 'trig_L2_calo_e237'          , 'F'), 
            #defineBranch( 'trig_L2_calo_e277'          , 'F'), 
            #defineBranch( 'trig_L2_calo_fracs1'        , 'F'), 
            #defineBranch( 'trig_L2_calo_weta2'         , 'F'), 
            #defineBranch( 'trig_L2_calo_ehad1'         , 'F'), 
            #defineBranch( 'trig_L2_calo_emaxs1'        , 'F'),            
            #defineBranch( 'trig_L2_calo_e2tsts1'       , 'F'), 
            #defineBranch( 'trig_L2_calo_wstot'         , 'F'),  
            #defineBranch( 'trig_L2_calo_energySample'  , 'vector<float>' ),  
            #defineBranch( 'trig_L2_calo_rings'         , 'vector<float>' ),         
          ]
    return branches







if __name__=='__main__':

    # ATR-11839 to fix the egammaPid import
    from PyUtils.Helpers import ROOT6Setup
    ROOT6Setup()

    # Setup the Run III behavior
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior = 1

    # Setup logs
    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import INFO
    log.setLevel(INFO)

    # Set the Athena configuration flags
    from AthenaConfiguration.AllConfigFlags import ConfigFlags

    path = '/afs/cern.ch/work/j/jodafons/public/mc21_13p6TeV.601189.PhPy8EG_AZNLO_Zee.recon.AOD.e8392_e7400_s3775_r13504_tid28458836_00'
    path+='/AOD.28458836._015067.pool.root.1'
    ConfigFlags.Input.Files = [path]
    ConfigFlags.Input.isMC = True
    ConfigFlags.Output.HISTFileName = 'TrigEgammaMonitorOutput.root'
    ConfigFlags.lock()

    # Initialize configuration object, add accumulator, merge, and run.
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg 
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    cfg = MainServicesCfg(ConfigFlags)
    cfg.merge(PoolReadCfg(ConfigFlags))

    def TrigEgammaDumperConfig(inputFlags, emulator=None):
        '''Function to configures some algorithms in the monitoring system.'''
        # The following class will make a sequence, configure algorithms, and link
        from AthenaMonitoring import AthMonitorCfgHelper
        helper = AthMonitorCfgHelper(inputFlags,'TrigEgammaAthMonitorCfg')
        from TrigEgammaMonitoring.TrigEgammaMonitoringMTConfig import TrigEgammaMonAlgBuilder
        monAlgCfg = TrigEgammaElectronDumperBuilder( helper, '2018' ) # Using 2018 e/g tunings
        monAlgCfg.configure()
        return helper.result()

    trigEgammaMonitorAcc = TrigEgammaDumperConfig(ConfigFlags)
    cfg.merge(trigEgammaMonitorAcc)
    # If you want to turn on more detailed messages ...
    #trigEgammaMonitorAcc.getEventAlgo('TrigEgammaMonAlg').OutputLevel = 2 # DEBUG
    cfg.printConfig(withDetails=False) # set True for exhaustive info

    cfg.run(20) #use cfg.run(20) to only run on first 20 events
